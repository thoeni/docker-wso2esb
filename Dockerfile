#
# WSO2 ESB 4.9.0 on CentOS Linux with Oracle JDK 1.7.0_80
#

FROM    thoeni/centos-oracle-java:1.8.0_60

MAINTAINER Antonio Troina, thoeni@gmail.com

ENV     WSO2_SOFT_VER=4.9.0

###     Download binary image from WSO2 official website
RUN     wget -P /opt --user-agent="testuser" --referer="http://connect.wso2.com/wso2/getform/reg/new_product_download" http://product-dist.wso2.com/products/enterprise-service-bus/${WSO2_SOFT_VER}/wso2esb-${WSO2_SOFT_VER}.zip
###     Update system
RUN     yum update -y
###     Install unzip to deflate the archive
RUN     yum install -y unzip
###     Unzip the archive into /opt directory
RUN     unzip /opt/wso2esb-${WSO2_SOFT_VER}.zip -d /opt
###     Move the folder removing the version
RUN     mv /opt/wso2esb-${WSO2_SOFT_VER} /opt/wso2esb
###     Remove the zip archive
RUN     rm /opt/wso2esb-${WSO2_SOFT_VER}.zip

###     Exporing console and service ports
EXPOSE  9443
EXPOSE  9763
EXPOSE  8243
EXPOSE  8280

##      Executing the server start
CMD     ["/opt/wso2esb/bin/wso2server.sh"]
